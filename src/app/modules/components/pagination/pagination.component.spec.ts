import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PaginationComponent } from './pagination.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('PaginationComponent', () => {
    let component: PaginationComponent;
    let fixture: ComponentFixture<PaginationComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PaginationComponent],
            imports: [RouterTestingModule],
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PaginationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should verify ngOnChanges() function', () => {
        component.ngOnChanges();
    });

    it('should verify getPageCount() function', () => {
        component.getPageCount();
    });

    it('should verify getArrayOfPage() function', () => {
        component.getArrayOfPage(1);
    });

    it('should verify onClickPage() function', () => {
        component.onClickPage(1);
    });
});
