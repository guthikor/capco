import { TableService } from '../../services/table.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TableComponent } from './table.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

class MockAppContextService {
  getMembersData() {
    return new Observable((observer) => {
      observer.next([]);
      observer.complete();
    });
  }
  postMembersData() { }
}

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TableComponent],
      imports: [RouterTestingModule],
      providers: [
        { provide: HttpClient },
        { provide: TableService, useClass: MockAppContextService }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should verify changeNumberPageRow() function', () => {
    component.changeNumberPageRow();
  });

  it('should verify changeDropdown() function', () => {
    component.changeDropdown();
  });

  it('should verify displayActivePage() function', () => {
    component.displayActivePage(1);
  });

  it('should verify postData() function', () => {
    component.postData('1', 'read');
  });

});
