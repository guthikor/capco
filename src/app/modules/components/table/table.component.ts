import { Component, OnInit } from '@angular/core';
import { TableService } from '../../services/table.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  membersData: any = [];
  activePage = 1;
  totalRecords = 0;
  recordsPerPage = 10;
  resJSONData: any = [];
  details: any;

  constructor(private tbService: TableService) { }

  ngOnInit() {
    this.tbService.getMembersData().subscribe(data => {
      this.resJSONData = JSON.parse(JSON.stringify(data));
      this.totalRecords = this.resJSONData.length;
      this.changeNumberPageRow();
    });
  }

  changeNumberPageRow() {
    const data = JSON.parse(JSON.stringify(this.resJSONData));
    this.membersData = data.slice((this.activePage - 1) * this.recordsPerPage, (this.activePage) * this.recordsPerPage);
  }

  changeDropdown() {
    this.activePage = 1;
  }

  displayActivePage(activePageNumber: number) {
    this.activePage = activePageNumber;
    this.changeNumberPageRow();
  }

  postData(id, status) {
    this.details = { id, status };
    this.tbService.postMembersData(this.details);
  }

}
