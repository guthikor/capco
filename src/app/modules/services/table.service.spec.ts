import { TestBed } from '@angular/core/testing';
import { TableService } from './table.service';
import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { defer } from 'rxjs';
let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy };
let tableService: TableService;
function asyncData<T>(data: T) {
    return defer(() => Promise.resolve(data));
}
describe('TableService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                { provide: HttpClient },
            ],
            schemas: [NO_ERRORS_SCHEMA]
        })
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        tableService = new TableService(<any>httpClientSpy);
    });

    it('should be created', () => {
        const service: TableService = TestBed.get(TableService);
        expect(service).toBeTruthy();
    });

    it('should return expected heroes (HttpClient called once)', () => {
        const expectedHeroes: any =
            [{ id: 1, name: 'A' }, { id: 2, name: 'B' }];
        httpClientSpy.get.and.returnValue(asyncData(expectedHeroes));
        tableService.getMembersData().subscribe(
            heroes => expect(heroes).toEqual(expectedHeroes, 'expected heroes'),
            fail
        );
        expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
    });

    xit('should return expected heroes (HttpClient called once)', () => {
        const expectedHeroes: any =
            [{ id: 1, name: 'A' }, { id: 2, name: 'B' }];
        httpClientSpy.post.and.returnValue(asyncData(expectedHeroes));
        tableService.postMembersData(expectedHeroes).subscribe(
            heroes => expect(heroes).toEqual(expectedHeroes, 'expected heroes'),
            fail
        );
        expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
    });

});
