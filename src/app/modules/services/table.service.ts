import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TableService {
  private jsonUrl = 'assets/sample_data.json';
  private apiUrl = 'api/submit';

  constructor(private http: HttpClient) { }

  getMembersData() {
    return this.http.get(this.jsonUrl);
  }

  postMembersData(data) {
    return this.http.post(this.apiUrl, data);
  }

}
